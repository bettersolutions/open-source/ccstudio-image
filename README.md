# ccstudio-image

```shell
# examples:

# run:
docker run -ti -v /workspace-dir:/workdir ccs_studio /bin/bash  

# import and build:
/opt/ti/ccs/eclipse/eclipse -noSplash -data "/workspace" -application com.ti.ccstudio.apps.projectImport -ccs.location /workdir/simple_peripheral_cc2640r2lp_app -ccs.autoImportReferencedProjects -ccs.autoBuild -ccs.copyIntoWorkspace

# import:
/opt/ti/ccs/eclipse/eclipse -noSplash -data "/workspace" -application com.ti.ccstudio.apps.projectImport -ccs.location /workdir/simple_peripheral_cc2640r2lp_stack_library

# build:
/opt/ti/ccs/eclipse/eclipse -noSplash -data "/workspace" -application com.ti.ccstudio.apps.projectBuild -ccs.workspace
```
