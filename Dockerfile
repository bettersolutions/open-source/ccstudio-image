FROM ubuntu:20.04
RUN dpkg --add-architecture i386 && apt-get update && apt-get install -y -o APT::Immediate-Configure=false libc6:i386 libusb-0.1-4 libgconf-2-4 libncurses5 libpython2.7 libtinfo5 udev

ENV HOME_DIR=/opt/ti
WORKDIR /ccs

# Download ti files
ADD https://software-dl.ti.com/ccs/esd/CCSv10/CCS_10_2_0/exports/CCS10.2.0.00009_linux-x64.tar.gz .
RUN tar -xvzf CCS10.2.0.00009_linux-x64.tar.gz
ADD https://software-dl.ti.com/codegen/esd/cgt_public_sw/TMS470/18.12.7.LTS/ti_cgt_tms470_18.12.7.LTS_linux-x64_installer.bin .
ADD https://software-dl.ti.com/dsps/dsps_public_sw/sdo_ccstudio/emulation/exports/ti_emupack_setup_9.2.0.00002_linux_x86_64.bin .
RUN chmod +x CCS10.2.0.00009_linux-x64/ccs_setup_10.2.0.00009.run ./*.bin

# Install CCStudio and dependencies
RUN CCS10.2.0.00009_linux-x64/ccs_setup_10.2.0.00009.run --prefix ${HOME_DIR} --enable-components PF_CC2X --mode unattended
RUN ./ti_cgt_tms470_18.12.7.LTS_linux-x64_installer.bin --prefix ${HOME_DIR} --mode unattended
RUN ./ti_emupack_setup_9.2.0.00002_linux_x86_64.bin --prefix ${HOME_DIR} --mode unattended
COPY ccs_files/simplelink_cc2640r2_sdk_4_30_00_08.run .
RUN ./simplelink_cc2640r2_sdk_4_30_00_08.run --prefix ${HOME_DIR} --mode unattended

# Use custom CC2640R2_LAUNCHXL
ENV CC2640R2_LAUNCHXL_PATH=${HOME_DIR}/simplelink_cc2640r2_sdk_4_30_00_08/source/ti/blestack/boards
RUN rm -r ${CC2640R2_LAUNCHXL_PATH}/CC2640R2_LAUNCHXL
ADD ccs_files/CC2640R2_LAUNCHXL.tar.xz ${CC2640R2_LAUNCHXL_PATH}

# Use custom osal_clock.h
ENV OSAL_PATH=${HOME_DIR}/simplelink_cc2640r2_sdk_4_30_00_08/source/ti/blestack/osal/src/inc
RUN rm ${OSAL_PATH}/osal_clock.h
COPY ccs_files/osal_clock.h ${OSAL_PATH}

# Use custom icall_addrs.h
ENV ICALL_PATH=${HOME_DIR}/simplelink_cc2640r2_sdk_4_30_00_08/source/ti/blestack/icall/inc
RUN rm ${ICALL_PATH}/icall_addrs.h
COPY ccs_files/icall_addrs.h ${ICALL_PATH}

# Clean up
RUN rm -r /ccs

# CCStudio config initialization
RUN mkdir /workspace
RUN ${HOME_DIR}/ccs/eclipse/eclipse -noSplash -data "/workspace" -application com.ti.common.core.initialize -ccs.toolDiscoveryPath "/opt/ti/simplelink_cc2640r2_sdk_4_30_00_08"

VOLUME /workdir
WORKDIR /workdir

ENTRYPOINT []
